# linux.aliases
# by kai germershausen @ GWDG

#alias l='ls'
#alias la='ls -a'
#alias ll='ls -l'
#alias lt='ls -lt'


alias l='exa'
alias la='exa -a'
alias ll='exa -l'
alias lla='exa -la'
alias lt='exa -lt modified --sort newest'
alias lta='exa -lat modified --sort newest'

alias h='history'

