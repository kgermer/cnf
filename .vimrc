" .vimrc
" by kai germershausen @ GWDG

" set defaults -----------------------------------------------
set nocompatible

filetype on
filetype plugin on
filetype indent on

syntax on

nnoremap <SPACE> <Nop>
let mapleader=" "
inoremap jj <ESC>

set shiftwidth=4
set tabstop=4
set expandtab

set incsearch
set hlsearch
set ignorecase
set smartcase
set showmatch


" plugin manager ----------------------------------------------
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin(has('vim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')

" declare plugins ---------------------------------------------
Plug 'tpope/vim-sensible'
Plug 'preservim/nerdtree'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'vim-airline/vim-airline'
Plug 'dense-analysis/ale'
Plug 'easymotion/vim-easymotion'

call plug#end()

" nerdtree settings -------------------------------------------
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>


