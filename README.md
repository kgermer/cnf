# cnf

Just a set of my default configuration settings; regarding tools i use on a regular basis.
These can be distributed to several hosts with techniques like 'dotfiles for git'.

**!CAUTION!**

**HAVE BACKUPS! STUFF MAY BREAK! USE AT YOUR OWN RISK!**


**requirements**

- GNU/Linux distribution (Debian/Ubuntu)
- bash/zsh
- git
- curl
- vim


## init

tl;dr -> bin/initial-dotfiles-cnf-setup.sh

    cd $HOME
    git clone --bare https://gitlab.gwdg.de/kgermer/cnf.git $HOME/.cnf

    alias cnf='/usr/bin/git --git-dir=$HOME/.cnf/ --work-tree=$HOME'
    cnf config --local status.showUntrackedFiles no
    echo ".cnf" >> $HOME/.gitignore

    # bash
    echo "alias cnf='/usr/bin/git --git-dir=$HOME/.cnf/ --work-tree=$HOME'" >> $HOME/.bashrc

    # zsh
    echo "alias cnf='/usr/bin/git --git-dir=$HOME/.cnf/ --work-tree=$HOME'" >> $HOME/.zshrc


## pull

    cnf pull

    cnf restore .aliases
    cnf restore .

