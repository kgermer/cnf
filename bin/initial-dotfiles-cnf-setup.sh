#!/bin/bash
# by kai germershausen @ GWDG

# clone cnf repository
git clone --bare https://gitlab.gwdg.de/kgermer/cnf.git $HOME/.cnf

# create dedicated git alias
alias cnf='/usr/bin/git --git-dir=$HOME/.cnf/ --work-tree=$HOME'
echo "alias cnf='/usr/bin/git --git-dir=$HOME/.cnf/ --work-tree=$HOME'" >> $HOME/.bashrc
echo "alias cnf='/usr/bin/git --git-dir=$HOME/.cnf/ --work-tree=$HOME'" >> $HOME/.zshrc

# show tracked files only
cnf config --local status.showUntrackedFiles no

# ignore our dotfiles git directory
echo ".cnf" >> $HOME/.gitignore

# restore / overwrite! lokal files
#cnf restore --staged .
cnf restore .aliases
cnf restore .

